package Koha::Plugin::Com::Theke::Coral;

# Copyright 2019 Theke Solutions
#
# This file is part of koha-plugin-gobi.
#
# koha-plugin-gobi is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-gobi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-gobi; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use utf8;

use Mojo::JSON qw(decode_json);

use base qw(Koha::Plugins::Base);

# Plugin version. Filled by the build tools.
our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'Coral connector for Koha',
    author          => 'Tomas Cohen Arazi',
    description     => 'Integrates Coral with Koha',
    date_authored   => '2019-11-05',
    date_updated    => '2019-11-05',
    minimum_version => '18.1100000',
    maximum_version => undef,
    version         => $VERSION,
};

=head1 Plugin methods

=head2 new

=cut

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;

    my $self = $class->SUPER::new($args);

    return $self;
}

=head2 configure

Method for handling the plugin configuration.

=cut

sub configure {
    my ( $self, $args ) = @_;
    my $cgi  = $self->{cgi};
    my $step = $cgi->param('step');

    my $template = $self->get_template({ file => 'configure.tt' });

    my $not_for_loan;

    if ( $step eq 'save' ) {
        $not_for_loan = $cgi->param('not_for_loan');
        $self->store_data(
            {
                not_for_loan => $not_for_loan,
            }
        );
    }
    else {
        $not_for_loan  = $self->retrieve_data( 'not_for_loan'  );
    }

    $template->param(
        not_for_loan  => $not_for_loan,
    );

    $self->output_html( $template->output() );
}

=head2 install

Method that is called when the plugin is installed.

=cut

sub install {
    my ( $self, $args ) = @_;

    my $po_table = $self->get_qualified_table_name('purchase_orders');

    C4::Context->dbh->do(qq{
        CREATE TABLE $po_table (
          `id` INT(11) NOT NULL auto_increment,
          `status` TEXT,
          `basketno` INT(11) REFERENCES aqbasket( basketno),
          `raw_msg` MEDIUMTEXT,
          `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY  (id),
          KEY basketno ( basketno),
          CONSTRAINT gobipo_basketno FOREIGN KEY ( basketno ) REFERENCES aqbasket ( basketno ) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    }) unless $self->table_exists($po_table);

    return 1;
}

=head2 api_routes

Method that returns the plugin's API routes

=cut

sub api_routes {
    my ( $self, $args ) = @_;

    my $spec_str = $self->mbf_read('openapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}

=head2 api_namespace

Method that returns the plugin's API namespace

=cut

sub api_namespace {
    my ($self) = @_;

    return 'coral';
}

=head1 Internal methods

=head2 table_exists

    unless ( $self->table_exists('name') ) {}

Internal implementation for checking table existence.

=cut

sub table_exists {
    my ($self,$table) = @_;
    eval {
        C4::Context->dbh->{PrintError} = 0;
        C4::Context->dbh->{RaiseError} = 1;
        C4::Context->dbh->do(qq{SELECT * FROM $table WHERE 1 = 0 });
    };
    return 1 unless $@;
    return 0;
}

1;
