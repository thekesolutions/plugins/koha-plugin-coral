package Koha::Plugin::Com::Theke::Coral::Controller;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Koha::Plugin::Com::Theke::Coral;

use Mojo::Base 'Mojolicious::Controller';

use Try::Tiny;

=head1 Koha::Plugin::Com::Theke::Coral::Controller

A class implementing the controller code for Coral requests

=head2 Class methods

=head3 add_order

Method that adds a new order from a Coral request

=cut

sub add_order {
    my $c = shift->openapi->valid_input or return;

    my $coral = Koha::Plugin::Com::Theke::Coral->new;

    my $body = $c->req->body;

    return try {

        my $order_id = 1; # TODO, should call some method that does something :-D

        $c->render(
            status  => 200,
            openapi => {
                order_id => $order_id
            }
        );
    }
    catch {

        return $c->render(
            status  => 500,
            openapi => { error => "Something went wrong. Check the logs." }
        );
    };
}

1;
